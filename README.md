
# Installation

> pip install launchpadlite

# USE

> from lplite import Lp
> 
> inkscape = Lp('inkscape')
>
> title = inkscape['title']
>
> image = inkscape.icon['image']
>
> user = inkscape.bug_supervisor
>
> count = len(inkscape.series_collection)

# WARNING

This library does not support oauth/write operations and has a lot of limitations. Use launchpadlib directly if you need those features. This is for people who can not use launchpadlib because of system requirements.

