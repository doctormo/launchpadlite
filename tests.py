#
# Copyright (C) 2016 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Some basic assert tests
"""

from lplite import Lp, Projects, Project, User, BugTask

import logging
logging.basicConfig()
logger = logging.getLogger('lplite')
logger.setLevel(logging.INFO)

class Author(Lp):
    known_url = '~doctormo'

user = Author()

assert(user['display_name'] == "Martin Owens")
assert(isinstance(user, Author))

inkscape = Lp('inkscape')

assert(inkscape['display_name'] == "Inkscape")
assert(inkscape.icon['image'])
assert(isinstance(inkscape, Project))

assert(isinstance(inkscape.driver, User))

bugs = inkscape.searchTasks(tags='layers')
tasks = list(bugs)
# This looks like it wouldn't test much, but len(bugs)
# comes from the entry count and list(..) comes from the
# paged content
assert(len(bugs) > 75)
assert(len(tasks) == len(bugs))
assert(isinstance(tasks[0], BugTask))

