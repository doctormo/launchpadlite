#!/usr/bin/env python
#
# Copyright (C) 2016 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#

from setuptools import setup
from lplite import __version__, __pkgname__
import os

# remove MANIFEST. distutils doesn't properly update it when the
# contents of directories change.
if os.path.exists('MANIFEST'): os.remove('MANIFEST')

# Grab description for Pypi
with open('README') as fhl:
    description = fhl.read()

# Used for rpm building
RELEASE = "1"

setup(
    name             = __pkgname__,
    version          = __version__,
    release          = RELEASE,
    description      = 'Very Light Access to Launchpad API',
    long_description = description,
    author           = 'Martin Owens',
    url              = 'https://github.com/doctormo/launchpadlite',
    author_email     = 'doctormo@gmail.com',
    platforms        = 'linux',
    license          = 'LGPLv3',
    py_modules       = ['lplite'],
    classifiers      = [
      'Development Status :: 5 - Production/Stable',
      'Development Status :: 6 - Mature',
      'Intended Audience :: Developers',
      'Intended Audience :: Information Technology',
      'Intended Audience :: System Administrators',
      'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
      'Operating System :: POSIX',
      'Operating System :: POSIX :: Linux',
      'Programming Language :: Python',
      'Programming Language :: Python :: 2.7',
    ],
    options = {
      'bdist_rpm': {
        'build_requires': [
          'python',
          'python-setuptools',
        ],
        'release': RELEASE,
      },
    },
 )

