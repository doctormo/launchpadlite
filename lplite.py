#
# Copyright (C) 2016 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
A library connecting to launchpad in the most useful and non-complex
way.

No caching support.
No authentication support.
Read only API
"""

__pkgname__ = 'launchpadlite'
__version__ = '0.7.5'

import re
import json
import urllib2
import logging
import functools

try:
    from urllib import urlencode
except ImportError:
    # PY3
    from urllib.parse import urlencode

#
# This URL hasn't changed in years, we're going to assume it's carved in stone.
#
SERVICE = 'https://api.launchpad.net/1.0/'
logger = logging.getLogger('lplite')


class combomethod(object):
    """Provide an interface for class and instance calls"""
    def __init__(self, method):
        self.method = method

    def __get__(self, obj=None, objtype=None):
	obj = objtype if obj is None else obj
        @functools.wraps(self.method)
        def _wrapper(*args, **kwargs):
	    return self.method(obj, *args, **kwargs)
        return _wrapper


class Lp(dict):
    """
    Base launchpad request object. Use directly:

    Lp(url)

    To make a request at that url, do not include the full domain, just the url
    after `/1.0/`.

    You can construct child classes which provide these three variables:

     * known_url - instances always have this url (for examine projects)
     * match_url - this class should always be used for this found url (regex)
     * match_op  - this class should always be used for this operator
                   even if the url would have matched. (e.g. searchTasks)
    """
    known_url = None
    match_url = None
    match_op = None

    def __new__(cls, url=None, data=None, **kw):
        """
        This will automatically assign an object's class to a matching url
        if found in the list of subclasses. This can be useful for loading
        sub objects like bug reports or users which will have the right type.
        """
        if data and 'self_link' in data and not url:
            url = data['self_link'][len(SERVICE):]
        if not url:
            return super(Lp, cls).__new__(cls)
        if '?' in url:
            (url, kwarg) = url.split('?', 1)
            # XXX We could parse kwargs here.
        for child in cls.subclasses():
            if cls is not child:
                if (child.match_url and re.match(child.match_url, url) and\
                    child.match_op == kw.get('ws.op', None)) or \
                   (child.known_url and url == child.known_url):
                    return child(url=url, data=data, **kw)
        return super(Lp, cls).__new__(cls)

    @classmethod
    def subclasses(cls):
        """Recursive access to subclasses list"""
        for child in cls.__subclasses__():
            yield child
            for descendant in child.subclasses():
                yield descendant

    def __init__(self, url=None, data=None, **kw):
        if data and 'self_link' in data and not url:
            url = data['self_link'][len(SERVICE):]
        self.url = url or self.known_url
        self.kw = kw
        self.loaded = False
        self.update(data or {})

    def __repr__(self):
        kind = type(self).__name__
        name = self.get_oid()
        if kind == 'Lp':
            if self.loaded:
                kind = self.get('resource_type_link', 'bad_api_type')
            else:
                kind = 'unknowntype'
        return "<Launchpad:%s '%s'>" % (kind, name)

    def __getitem__(self, key):
        self.load()
        return super(Lp, self).__getitem__(key)

    def get(self, *args, **kw):
        self.load()
        return super(Lp, self).get(*args, **kw)

    def keys(self):
        self.load()
        return super(Lp, self).keys()

    def update(self, data):
        """Gets all the data items and filters them"""
        for (key, value) in data.items():
            if isinstance(value, (str, unicode)):
                value = self.filter(key, value)
            self[key] = value
        # If there was no data, set loaded to false, useful for
        # resetting the internal cache when needed.
	self.loaded = bool(data)

    def filter(self, key, value):
        if value.startswith(SERVICE+'#'):
            return value.split('#')[-1]
        if value.startswith(SERVICE) and key.endswith('_link'):
            name = key[:-5]
            if not hasattr(self, name) and name != 'self':
                setattr(self, name, Lp(url=value[len(SERVICE):]))
        return value

    @combomethod
    def get_url(cls, **kw):
        """Get which ever url is the most useful"""
        url = cls.known_url
        if isinstance(cls, Lp):
            url = cls.url
	    kw.update(cls.kw)
        if not url:
            raise IOError("No URL set for data")
        return SERVICE + url + cls.backend_kw(**kw)

    def get_oid(self):
        """Tries to return a unique part of the url, or returns the url"""
        if self.match_url:
            match = re.match(self.match_url, self.url)
            return match.groupdict({}).get('id', self.url)
        return self.url

    def load(self):
        """Loads data from launchpad based on this object's URL"""
        if self.loaded:
            return
        self.update(self._load(self.get_url()))
        logger.info("Loading complete.")

    @classmethod
    def _load(cls, url):
        """Load any url from the API and return a python dictionary"""
        logger.info("Getting URL: %s" % url)
        try:
            dat = urllib2.urlopen(url).read()
        except urllib2.HTTPError as err:
            if err.code == 400:
                logging.error("API Error: %s" % err.read())
                return {}
            raise
        logger.info("Got %d bytes of data" % len(dat))
        if dat.startswith('\x89\x50\x4e\x47'):
            logger.info("Found image data.")
            return {'image': dat}
        try:
            return json.loads(dat)
        except ValueError:
            logger.error("Data is broken: " + dat[:200])
            return {}

    def entries(self, data):
        """Page the entries we have"""
        for item in data['entries']:
            yield item
        if 'next_collection_link' in data:
            url = data['next_collection_link'].replace('ws.size=1', 'ws.size=75')
            for item in self.entries(self._load(url)):
                yield item

    def __len__(self):
        """Get the count of entries in this iter"""
        if not self.loaded:
            # We ALWAYS ask for one item by default to get the size
            # But this is called when doing a list(), so it's better
            # to iterate directly rather than make a list.
            self.kw['ws.size'] = 1
        return self.get("total_size", 0)

    def __iter__(self):
        """Looks for entries to list through"""
        self.load()
        keys = list(super(Lp, self).__iter__())
        if 'entries' in keys:
            for item in self.entries(self):
                yield Lp(data=item)
        else:
            for item in keys:
                yield item

    @combomethod
    def operation(cls, name, **kw):
        """Generic way of calling a launchpad api function"""
        if isinstance(cls, Lp):
            kw['url'] = cls.url
            cls = type(cls)
        kw['ws.op'] = name
        return cls(**kw)

    def frontend_kw(self):
        """Returns query kwargs in terms of the front end url"""
        def noop(name, value):
            return (name, value)
        for (name, value) in self.kw.items():
            op = getattr(self, 'kw_filter_'+name, noop)
            if name.startswith('ws.'):
                continue
            if isinstance(value, bool):
                value = value and "on" or None
            if isinstance(value, list) and name:
                for v in value:
                    yield "field.%s%%3Alist=%s" % op(name, v)
            elif value and name:
                yield "field.%s=%s" % op(name, value)

    def kw_filter_tags(self, name, value):
        return ('tag', value)

    def kw_filter_status(self, name, value):
        value = value.upper().replace("'", '')
        if '(' in value:
            return ('status', value.replace('(','')[:-1].replace(' ', '_'))
        return ('status', value.replace(' ', ''))

    @combomethod
    def backend_kw(self, **kw):
        if isinstance(self, Lp):
            kw.update(self.kw)
        if kw:
            return '?' + urlencode(kw, 1)
        return ''

    # searchTasks is available on so many API branches, we're putting it
    # here on the parent class instead of on each of the child classes below.
    searchTasks = lambda cls, **kw: cls.operation('searchTasks', **kw)

    def web_link(self):
        """Returns a launchpad front-end link (or none)"""
        if self.kw.get('ws.op', None) == 'searchTasks':
            # Special link to bug tracker searches (matches searchTasks above)
            url = Lp(self.url)['web_link']
            return url + '/+bugs?' + '&'.join(self.frontend_kw())
        return self.get('web_link', None)


# ========== LAUNCHPAD API SPECIFIC ============= #

class User(Lp):
    match_url = r'^~(?P<id>[^\/]+)$'

class Project(Lp):
    # This match is so broad, it'd going to be constantly fighting the api
    match_url = r'^(?P<id>(?!projects|people|\\.)[^\/~]+)$'

class People(Lp):
    known_url = 'people'
    find = lambda cls, **kw: cls.operation('find', **kw)
    findPerson = lambda cls, **kw: cls.operation('findPerson', **kw)
    findTeam = lambda cls, **kw: cls.operation('findTeam', **kw)
    getByEmail = lambda cls, **kw: cls.operation('getByEmail', **kw)

class Projects(Lp):
    known_url = 'projects'
    search = lambda cls, **kw: cls.operation('search', **kw)

class BugTask(Lp):
    match_url = r'^[^\/]+/\+bug/(?P<id>\d+)$'

class Milestone(Lp):
    match_url = r'^[^\/]+/\+milestone/(?P<id>[^\/]+)'


class ProjectTasks(Project):
    match_op = 'searchTasks'

class MilestoneTasks(Milestone):
    match_op = 'searchTasks'

    def web_link(self):
        # Special link to bug tracker searches
        project = Lp(self.url.rsplit('/', 2)[0])
        url = project['web_link']

        # This horrible, horrible hack, brought to you by the launchpad API,
        # the problem can be summed as: Milestones in bug searches use an
        # internal id that is gotten NOWHERE else in the entire API.
        logger.info("Hacking into launchpad frontend for milestone data")
        dat = urllib2.urlopen(url + '/+bugs?advanced=1').read()
        # Look at the advanced bugs HTML and scrape the milestone internal ids
        dat = re.findall(r'name="field.milestone:list"\s+type="checkbox"\s+id='
         r'"milestone.([^"(]+)( \(\d{4}-\d{2}-\d{2}\))?"\s+value="(\d+)"', dat)
        keys = dict([(name, pk) for (name, dt, pk) in dat])
        # Get the milestone name, yep, take a look at how even the API resists
        # giving a value for the title, adding in the codename for no reason.
        name = Lp(self.url)['title'].rsplit(' "')[0]
        kw = ["field.milestone%3Alist=" + keys[name]]
        return url + '/+bugs?' + '&'.join(list(self.frontend_kw()) + kw)

